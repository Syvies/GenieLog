/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.us997606.panier;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author us997606
 */
public class PanierTest {
    
    public PanierTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setMaxOranges method, of class Panier.
     */
    @org.junit.Test
    public void testSetMaxOranges() {
        System.out.println("setMaxOranges");
        int maxOranges = 10;
        Panier instance = new Panier(0);
        instance.setMaxOranges(maxOranges);
        assertEquals(maxOranges, instance.getMaxOranges());
    }

    /**
     * Test of getMaxOranges method, of class Panier.
     */
    @org.junit.Test
    public void testGetMaxOranges() {
        System.out.println("getMaxOranges");
        Panier instance = new Panier(0);
        int expResult = 0;
        int result = instance.getMaxOranges();
        assertEquals(expResult, result);
    }

    /**
     * Test of estPlein method, of class Panier.
     */
    @org.junit.Test
    public void testEstPlein() throws Exception {
        System.out.println("estPlein");
        Panier instance = new Panier(1);
        boolean expResult = false;
        boolean result = instance.estPlein();
        assertEquals(expResult, result);

        instance.ajouteOrange(new Orange(0.0, "Free test orange"));
        
        expResult = true;
        result = instance.estPlein();
        assertEquals(expResult, result);
    }

    /**
     * Test of estVide method, of class Panier.
     */
    @org.junit.Test
    public void testEstVide() {
        System.out.println("estVide");
        Panier instance = new Panier(0);
        boolean expResult = true;
        boolean result = instance.estVide();
        assertEquals(expResult, result);
    }

    /**
     * Test of ajouteOrange method, of class Panier.
     */
    @org.junit.Test
    public void testAjouteOrange() throws Exception {
        System.out.println("ajouteOrange");
        Orange o = new Orange(0.0, "Free test orange");
        Panier instance = new Panier(1);
        instance.ajouteOrange(o);
        assertEquals(instance.estPlein(), true);
    }

    /**
     * Test of retire method, of class Panier.
     */
    @org.junit.Test
    public void testRetire() throws Exception {
        System.out.println("retire");
        Panier instance = new Panier(1);
        Orange o = new Orange(0.0, "Free test orange");
        instance.ajouteOrange(o);
        instance.retire();
        assertEquals(instance.estPlein(), false);
        assertEquals(instance.estVide(), true);
    }

    /**
     * Test of getPrix method, of class Panier.
     */
    @org.junit.Test
    public void testGetPrix() throws Exception {
        System.out.println("getPrix");
        Panier instance = new Panier(1);
        instance.ajouteOrange(new Orange(0.0, "Free test orange"));
        double expResult = 0.0;
        double result = instance.getPrix();
        assertEquals(expResult, result, 0.0);
        
        
        instance.ajouteOrange(new Orange(2.53, "Free test orange"));
        expResult = 2.53;
        result = instance.getPrix();
        assertEquals(expResult, result, 2.53);
    }

    /**
     * Test of boycotteOrigine method, of class Panier.
     */
    @org.junit.Test
    public void testBoycotteOrigine() throws Exception {
        System.out.println("boycotteOrigine");
        String origine = "Belgique";
        Panier instance = new Panier(1);
        Orange o = new Orange(0.0, origine);
        instance.ajouteOrange(o);
        instance.boycotteOrigine(origine);
        assertEquals(instance.estVide(), true);
    }
    
}
