/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.us997606.panier;

import java.util.ArrayList;
/**
 *
 * @author us997606
 */
public class Panier {
    private ArrayList<Orange> oranges;
    private int maxOranges;
    
    public Panier(int maxOranges) {
        oranges = new ArrayList<>();
        setMaxOranges(maxOranges);
    }
    
    public void setMaxOranges(int maxOranges) {
        this.maxOranges = maxOranges;
    }
    
    public int getMaxOranges() {
        return this.maxOranges;
    }
    
    public boolean estPlein() {
        return oranges.size() >= maxOranges;
    }
    
    public boolean estVide() {
        return oranges.isEmpty();
    }
    
    @Override
    public String toString() {
        String s = "Panier :\n============\n";
        for(int i = 0; i< oranges.size(); i++){
            s += oranges.toString() + "============\n";
        }
        s += "Total : " + this.getPrix() + " €\n============\n";
        return s;
    }
    
    public void ajouteOrange(Orange o) {
        if (!this.estPlein()){
            oranges.add(o);
        }
    }
    
    public void retire() {
        if (!this.estVide()){
            oranges.remove(oranges.size()-1);
        }
    }
    
    public double getPrix() {
        double res = 0.0;
        for(int i = 0; i< oranges.size(); i++){
            res += oranges.get(i).getPrix();
        }
        return res;
    }
    
    public void boycotteOrigine(String origine) {
        for(int i = oranges.size() - 1; i >= 0 ; i--){
            if(oranges.get(i).getOrigine() == origine) {
                oranges.remove(i);
            }
        }
    }
}
