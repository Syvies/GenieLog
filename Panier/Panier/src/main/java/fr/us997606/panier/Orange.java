/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.us997606.panier;

/**
 *
 * @author us997606
 */
public class Orange {
    private double prix;
    private String origine;
    
    public Orange(double prix, String origine) throws Exception {
        setPrix(prix);
        setOrigine(origine);
    }
    
    public void setPrix(double prix) throws Exception {
        if (prix < 0) {
            throw new Exception("Prix inférieur à 0");
        }
        this.prix = prix;
    }
    
    public double getPrix() {
        return this.prix;
    }
    
    public void setOrigine(String origine) {
        this.origine = origine;
    }
    
    public String getOrigine() {
        return this.origine;
    }
    
    public String toString() {
        String s;
        s = "Orange\nPrix : " + this.getPrix() + " €\nOrigine : " + this.getOrigine() + "\n";
        return s;
    }
}
